Password Roller
===============
Role to update passwords of existing user accounts on the remote machine. This Role uses the passwd file to confirm user exist and will not create the user.

Requirements
------------
None

Role Variables
--------------

| Variable  | Type  |Required  | Descirption  |
|---|---|---|---|
|  theUser | String  | Yes  | The username of the account to update  |
| new_password  | String  | Yes  |  The new password for the account updated. Password must be hashed such as md5 or sha512 See: http://docs.ansible.com/ansible/latest/faq.html#how-do-i-generate-crypted-passwords-for-the-user-module |


Dependencies
------------
None

Example Playbook
----------------

**Playbook that will ask you for the user and pass**
```yaml
- name: Password Rollover
  hosts:
  become: yes
  gather_facts: false
  vars_prompt:
    - name: "theUser"
      prompt: "Enter user Name:"
      private: no
 
    - name: "new_password"
      prompt: "Enter New Password"
      private: yes
      encrypt: "sha512_crypt"
      confirm: yes
      salt_size: 7

    - hosts: servers
      roles:
         - { role: cblack34.password-roller }
```

**Playbook that will update multiple users and passwords**
```yaml
- name: Password Rollover
  hosts:
  become: yes
  gather_facts: false

  roles:
     - { role: cblack34.password-roller, theUser: root, new_password: $6$V3GdAmLA0Wg.maZs$kspL1MZrFNm4i01qHDunAgtWYOqmWJbYCuT0eBDaL3yts5Q04MA2it6xB3cO144NbE3iW3CO86YIQlYyjetBP1 }
     - { role: cblack34.password-roller, theUser: user2, new_password: $6$V3GdAmLA0Wg.maZs$kspL1MZrFNm4i01qHDunAgtWYOqmWJbYCuT0eBDaL3yts5Q04MA2it6xB3cO144NbE3iW3CO86YIQlYyjetBP1 }
```

Notes
-----
The easy way to get a hashed password is:

**Make sure you have passlib installed**
```
pip install passlib
```

**Run this command**
change *sha512_crypt* to reflect your systems password hash.
```
python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.using(rounds=5000).hash(getpass.getpass())"
```

To Do
-----
- Change role to be idempotent. Probably through comparing the shadown and new hashed password.
- Change variables to have the roll name prefix.

License
-------

BSD

Author Information
------------------

Clayton Black
cblack@claytontblack.com
http://www.claytontblack.com

